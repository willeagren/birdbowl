/*
    Code to switch image
*/
let my_image = document.querySelector('img');

my_image.onclick = function() {
    let my_src = my_image.getAttribute('src');
    if(my_src === 'images/Birdbowl-icon.png') {
        my_image.setAttribute ('src', 'images/Birdbowl-icon.jpg');
    } else {
        my_image.setAttribute('src', 'images/Birdbowl-icon.png');
    }
}

/*
    Code to personalize the welcome message for the website
*/

let my_button = document.querySelector('button');
let my_heading = document.querySelector('h1');

function set_user_name() {
    let my_name = prompt('Please enter your name.');
    if(!my_name || my_name === NULL) {
        set_user_name();
    } else {
        localStorage.setItem('name', my_name);
        my_heading.textContent = 'Welcome to Birdbowl docs, ' + my_name;
    }
}

if(!localStorage.getItem('name')) {
    set_user_name();
} else {
    let stored_name = localStorage.getItem('name');
    my_heading.textContent = 'Welcome to Birdbowl docs, ' + stored_name;
}

my_button.onclick = function() {
    set_user_name();
}
